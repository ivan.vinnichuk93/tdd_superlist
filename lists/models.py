from django.db import models
from django.urls import reverse

class List(models.Model):
	'''list of items'''

	def get_absolute_url(self):
		'''get absolute url'''
		return reverse('view_list', args=[self.id])


class Item(models.Model):
	'''element of list'''
	text = models.TextField(default='', blank=False)
	list = models.ForeignKey(List, default=None, on_delete=None)

	class Meta:
		ordering = ('id',)
		unique_together = ('list', 'text')

	def __str__(self):
		return self.text


