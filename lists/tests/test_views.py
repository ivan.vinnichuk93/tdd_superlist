from django.urls import resolve
from django.test import TestCase
from django.http import HttpRequest
from django.template.loader import render_to_string
from django.utils.html import escape
from lists.forms import ItemForm, EMPTY_ITEM_ERROR
from lists.models import Item, List
from unittest import skip
from lists.forms import (
DUPLICATE_ITEM_ERROR, EMPTY_ITEM_ERROR,
ExistingListItemForm, ItemForm
)

class HomePageTest(TestCase):
    '''Test for home page'''

    def test_uses_home_templates(self):
        '''test: uses home templates'''
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'home.html')

    def test_home_page_uses_item_form(self):
        '''test home page uses item form'''
        response = self.client.get('/')
        self.assertIsInstance(response.context['form'], ItemForm)    # check if our form has a correct class


class ListViewTest(TestCase):
    '''test for list view'''

    def test_uses_list_template(self):
        '''test: usage list template'''
        empty_list = List.objects.create()
        response = self.client.get(f'/lists/{empty_list.id}/')
        self.assertTemplateUsed(response, 'list.html')

    def test_displays_only_items_for_that_list(self):
        '''test: show all element of the list'''
        correct_list = List.objects.create()
        Item.objects.create(text='Item 1', list=correct_list)
        Item.objects.create(text='Item 2', list=correct_list)
        other_list = List.objects.create()
        Item.objects.create(text='other Item 1', list=other_list)
        Item.objects.create(text='other Item 2', list=other_list)
        response = self.client.get(f'/lists/{correct_list.id}/')

        self.assertContains(response, 'Item 1')
        self.assertContains(response, 'Item 2')
        self.assertNotEqual(response, 'other Item 1')
        self.assertNotEqual(response, 'other Item 2')

    def test_passes_corect_list_to_template(self):
        '''test passes correct list to template'''
        other_list = List.objects.create()
        correct_list = List.objects.create()
        response = self.client.get(f'/lists/{correct_list.id}/')
        self.assertEqual(response.context['list'], correct_list)

    def test_can_save_a_POST_request_to_an_existing_list(self):
        '''test can save new item in the existing list'''
        other_list = List.objects.create()
        correct_list = List.objects.create()

        self.client.post(f'/lists/{correct_list.id}/', data={'text': 'A new item for an existing list'})
        self.assertEqual(Item.objects.count(), 1)
        new_item = Item.objects.first()
        self.assertEqual(new_item.text, 'A new item for an existing list')
        self.assertEqual(new_item.list, correct_list)

    def test_POST_redirects_to_list_view(self):
        '''test redirect to list view'''
        other_list = List.objects.create()
        correct_list = List.objects.create()

        response = self.client.post(f'/lists/{correct_list.id}/',
                                    data={'text': 'A new item for an existing list'})
        self.assertRedirects(response, f'/lists/{correct_list.id}/')

    def post_invalid_input(self):
        '''post invalid_input'''
        list_ = List.objects.create()
        return self.client.post(
            f'/lists/{list_.id}/',
            data={'text': ''}
        )

    def test_for_invalid_input_nothing_saved_to_db(self):
        '''test for invalid input nothing saved to db'''
        self.post_invalid_input()
        self.assertEqual(Item.objects.count(), 0)

    def test_for_invalid_input_renders_list_template(self):
        '''test for invalid input renders list template'''
        response = self.post_invalid_input()
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'list.html')

    def test_for_invalid_input_passes_form_to_template(self):
        '''test for invalid input passes form to template'''
        response = self.post_invalid_input()
        self.assertIsInstance(response.context['form'], ExistingListItemForm)

    def test_for_invalid_input_shows_error_on_page(self):
        '''test for invalid input shows error on page'''
        response = self.post_invalid_input()
        self.assertContains(response, escape(EMPTY_ITEM_ERROR))

    def test_duplicate_item_validation_errors_and_up_on_lists_page(self):
        '''test duplicate item validation errors and up on lists page'''
        expected_error = escape(DUPLICATE_ITEM_ERROR)
        list1 = List.objects.create()
        item1 = Item.objects.create(list=list1, text='textey')
        response = self.client.post(
            f'/lists/{list1.id}/',
            data={'text': 'textey'}
        )
        # expected_error = escape("You've already got this in your list")
        self.assertContains(response, expected_error)
        self.assertTemplateUsed(response, 'list.html')
        self.assertEqual(Item.objects.all().count(), 1)


class NewListTest(TestCase):
    '''test of new list'''

    def test_can_save_a_POST_request(self):
        '''test: can save post request'''
        self.client.post('/lists/new', data={'text': 'A new list item'})
        self.assertEqual(Item.objects.count(), 1)
        new_item = Item.objects.first()
        self.assertEqual(new_item.text, 'A new list item')

    def test_redirect_after_POST(self):
        '''test redirect after POST request'''
        response = self.client.post('/lists/new', data={'text': 'A new list item'})
        new_list = List.objects.first()
        self.assertRedirects(response, f'/lists/{new_list.id}/')

    # def test_validation_errors_are_sent_back_to_home_page_template(self):
    #     '''test: validation errors are sent back to the home page template'''
    #     response = self.client.post('/lists/new', data={'text': ''})
    #     self.assertEqual(response.status_code, 200)
    #     self.assertTemplateUsed(response, 'home.html')
    #     expected_error = escape("You can't have an empty list item")
    #     self.assertContains(response, expected_error)

    def test_for_invalid_renders_home_template(self):
        '''test for invalid renders home template'''
        response = self.client.post('/lists/new', data={'text': ''})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'home.html')

    def test_validation_errors_are_shown_on_home_page(self):
        '''test validation errors are shown on home page'''
        response = self.client.post('/lists/new', data={'text': ''})
        self.assertContains(response, escape(EMPTY_ITEM_ERROR))

    def test_for_invalid_passes_form_to_template(self):
        '''test for invalid passes form to template'''
        response = self.client.post('/lists/new', data={'text': ''})
        self.assertIsInstance(response.context['form'], ItemForm)

    def test_invalid_list_items_arent_saved(self):
        '''test: saved invalid list items'''
        self.client.post('list/new', data={'text': ''})
        self.assertEqual(List.objects.count(), 0)
        self.assertEqual(List.objects.count(), 0)
        self.assertEqual(Item.objects.count(), 0)

    def test_displays_item_form(self):
        '''test displays item form'''
        list_ = List.objects.create()
        response = self.client.get(f'/lists/{list_.id}/')

        self.assertIsInstance(response.context['form'], ExistingListItemForm)
        self.assertContains(response, 'name="text"')


class ExistingListItemFormTest(TestCase):
    '''test form element in existing list'''

    def Test_form_renders_item_text_input(self):
        '''test form renders item text input'''
        list_ = List.objects.create()
        form = ExistingListItemForm(for_list=list_)
        self.assertIn('placeholder="Enter a to-do item"', form.as_p())

    def test_form_validation_for_blank_items(self):
        '''test form validation for blank items'''
        list_ = List.objects.create()
        form = ExistingListItemForm(for_list=list_, data={'text': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['text'], [EMPTY_ITEM_ERROR])

    def test_form_validation_for_duplicate_items(self):
        '''test form validation for duplicate items'''
        list_ = List.objects.create()
        Item.objects.create(list=list_, text='no-twins!')
        form = ExistingListItemForm(for_list=list_, data={'text': 'no-twins!'})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['text'], [DUPLICATE_ITEM_ERROR])

