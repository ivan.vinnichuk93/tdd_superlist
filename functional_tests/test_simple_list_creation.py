from .base import FunctionalTest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class NewVisitorTest(FunctionalTest):
	'''тест нового користувача'''

	def test_can_start_a_list_for_one_user(self):
		''' тест: можна розпочати список та отримати його пізніше'''

		# Едіт чула про крутий онлайн-додаток зі списком
		# важливих справ. Вона вирішує оцінити його головну сторінку
		self.browser.get(self.live_server_url)

		# Вона бачить, що заголовок та шапка сторінки говорить про список невідкладних справ
		self.assertIn('To-Do', self.browser.title)
		header_text = self.browser.find_element_by_tag_name('h1').text
		self.assertIn('To-Do',header_text)

		# Їй відразу пропонується ввести елемент списку
		input_box = self.get_item_input_box()
		self.assertEqual(
			input_box.get_attribute('placeholder'),
			'Enter a to-do item'
		)

		# Вона набирає в текстовосу полі "Купити пр'я павлина"
		# Її хоббі в'язання риболовних мушок
		input_box.send_keys('Buy peacock feathers')

		# Коли вона натискає enter сторінка оновлюється, і тепер сторінка
		# містить "1: Купити пір'я павлина" в якості елемента списка
		input_box.send_keys(Keys.ENTER)
		self.wait_for_row_in_list_table('1:Buy peacock feathers')

		# Текстове поле все ще пропонує їй додати ще один елемент.
		# Вона вводить "Зробити мушку з пір1я"
		# Едіт дуже методична
		inputbox = self.get_item_input_box()
		inputbox.send_keys('Make a Peacock Feather')
		inputbox.send_keys(Keys.ENTER)


		# Сторінка оновлюється, і тепер видна два елементи в її списку

		self.wait_for_row_in_list_table('2:Make a Peacock Feather')
		self.wait_for_row_in_list_table('1:Buy peacock feathers')

		# Едіт цікаво, чи запам'ятає сайт її список.  Далі вона бачить, що
		#сайт згенерував для неї унікальну URL-адресу - про це 
		# виводиться невеликий текст з поясненням
		# self.fail('Закінчити тест!')
		# Вона відвідує цю URL-адресу - її список все ще там.

		# Задоволена вона знову лягає спати.

	def test_multiple_users_can_start_lists_at_different_urls(self):
		'''test: multiple_users can start lists at different urls'''
		# Edit start a new list
		self.browser.get(self.live_server_url)
		inputbox = self.get_item_input_box()
		inputbox.send_keys('Buy peacock feathers')
		inputbox.send_keys(Keys.ENTER)
		self.wait_for_row_in_list_table('1:Buy peacock feathers')

		# She notices that her list has a unique URL
		edith_list_url = self.browser.current_url
		self.assertRegex(edith_list_url, '/lists/.+')

		# Now a new user Frensis is commin to the site.

		# We are using a new browser session, thereby ensuring that no information
		# from Edith passes through cookie data, etc.
		self.browser.quit()
		self.browser = webdriver.Firefox()

		# Francis visits the homepage. There is no sign of Edith’s list.
		self.browser.get(self.live_server_url)
		page_text = self.browser.find_elements_by_tag_name('body')[0].text
		self.assertNotIn('Buy peacock feathers', page_text)
		self.assertNotIn('Make a Peacock Feather', page_text)

		# Francis begins a new list by introducing a new item. He's less interesting than Edith's list ...
		inputbox = self.get_item_input_box()
		inputbox.send_keys('Buy milk')
		inputbox.send_keys(Keys.ENTER)
		self.wait_for_row_in_list_table('1:Buy milk')

		# Francis gets a unique URL
		francis_list_url = self.browser.current_url
		self.assertRegex(francis_list_url, '/lists/.+')
		self.assertNotEqual(francis_list_url, edith_list_url)

		# Again, there is no sign of Edith’s list.
		page_text = self.browser.find_elements_by_tag_name('body')[0].text
		self.assertNotIn('Buy peacock feathers', page_text)
		self.assertIn('Buy milk', page_text)



