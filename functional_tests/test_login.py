from django.core import mail
from selenium.webdriver.common.keys import Keys
import re

from .base import FunctionalTest

TEST_EMAIL = 'test@example.com'
SUBJECT = 'Your login lilnk for Superlists'

class LoginTest(FunctionalTest):
	'''testfor registaration in system'''

	def test_can_get_email_link_to_log_in(self):
		'''test can get email lilnk to log in'''
		# Edit enter her email to fileld
		self.browser.get(self.live_server_url)
		self.browser.find_element_by_name('email').send_keys(TEST_EMAIL)
		self.browser.find_element_by_name('email').send_keys(Keys.ENTER)

		# she see a message about sending mail to her email
		self.wait_for(lambda: self.assertIn(
			'Check your email',
			self.browser.find_element_by_tag_name('body').text
		))

		# Edit check her email and find there a leter
		email = mail.outbox[0]
		self.assertIn(TEST_EMAIL, email.to)
		self.assertEqual(email.subject, SUBJECT)

		# It has link to url
		self.assertIn('Use this link to log in', email.body)
		url_search = re.search(r'http://.+/.+$', email.body)
		if not url_search:
			self.fail(f'Could not find url in email body:\n{email.body}')
		url = url_search.group(0)
		self.assertIn(self.live_server_url, url)

		# Edit click to link
		self.browser.get(url)

		# She authenticate in system
		self.wait_for(
			lambda: self.browser.find_element_by_link_text('Log out')
		)
		navbar = self.browser.find_element_by_css_selector('.navbar')
		self.assertIn(TEST_EMAIL, navbar.text)
