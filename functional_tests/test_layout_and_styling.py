from selenium.webdriver.common.keys import Keys
from .base import FunctionalTest


class LaypoutAndStylingTest(FunctionalTest):
	'''test for maket and include style files'''
	
	def test_layout_and_styling(self):
		'''test layout and styling'''
		# Edit open main page
		self.browser.get(self.live_server_url)
		self.browser.set_window_size(1024, 768)

		# She notices that the input box is neatly centered
		input_box = self.get_item_input_box()
		self.assertAlmostEqual(
			input_box.location['x'] + input_box.size['width'] / 2,
			512,
			delta=10
		)

		# She start a new list and see that input field centered there too
		input_box.send_keys('testing')
		input_box.send_keys(Keys.ENTER)
		self.wait_for_row_in_list_table('1:testing')
		inputbox = self.get_item_input_box()
		self.assertAlmostEqual(
			inputbox.location['x'] + inputbox.size['width'] / 2,
			512,
			delta=10
		)


