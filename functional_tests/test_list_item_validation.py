from selenium.webdriver.common.keys import Keys
from .base import FunctionalTest


class ItemValidationTest(FunctionalTest):
	'''test for validation list item'''
	def get_error_element(self):
		'''get element with error'''
		return self.browser.find_element_by_css_selector('.has-error')


	def test_cannot_add_empty_list_items(self):
		'''test: can not add empty item'''

		# Edit open home page and try accidentally send emptyitem. She submit with empty field
		self.browser.get(self.live_server_url)
		self.get_item_input_box().send_keys(Keys.ENTER)

		# browser intercept request and do not load page with list
		self.wait_for(lambda: self.browser.find_elements_by_css_selector('#id_text:invalid'))

		# Home page relod and on the screen appears the message about error, which say about field can't be empty
		# self.wait_for(lambda: self.assertEqual(
		# 	self.browser.find_element_by_css_selector('.has-error').text,
		# 	"You can't have an empty list item"
		# ))

		# She try again, now with some message for element, and now it works
		self.get_item_input_box().send_keys('Buy milk')
		self.wait_for(lambda: self.browser.find_elements_by_css_selector('#id_text:valid'))
		self.get_item_input_box().send_keys(Keys.ENTER)
		self.wait_for_row_in_list_table('1:Buy milk')

		# Syrprisingly,  Edit dicidet to send a second empty list item
		self.get_item_input_box().send_keys(Keys.ENTER)

		# She see the same error message
		# self.wait_for(lambda: self.assertEqual(
		# 	self.browser.find_element_by_css_selector('.has-error').text,
		# 	"You can't have an empty list item"
		# ))
		self.wait_for(lambda: self.browser.find_elements_by_css_selector('#id_text:invalid'))
		# And She can fix it by writing some text to the field
		self.get_item_input_box().send_keys('Make tea')
		self.wait_for(lambda: self.browser.find_elements_by_css_selector('#id_text:valid'))
		self.get_item_input_box().send_keys(Keys.ENTER)
		self.wait_for_row_in_list_table('1:Buy milk')
		self.wait_for_row_in_list_table('2:Make tea')

	def test_cannot_add_duplicate_items(self):
		'''test can not add duplicate items'''

		# Edit open home page and start new list
		self.browser.get(self.live_server_url)
		self.get_item_input_box().send_keys('Buy wellies')
		self.get_item_input_box().send_keys(Keys.ENTER)
		self.wait_for_row_in_list_table('1:Buy wellies')

		# She accidentally tries to introduce a repeating element.
		self.get_item_input_box().send_keys('Buy wellies')
		self.get_item_input_box().send_keys(Keys.ENTER)

		# She sees a useful error message.

		self.wait_for(lambda: self.assertEqual(
			self.get_error_element().text,
			"You've already got this in your list"
		))

	def test_error_message_are_cleared_on_input(self):
		'''test error message are cleared on input'''
		# Edit start list and make validation error
		self.browser.get(self.live_server_url)
		self.get_item_input_box().send_keys('Banter too thick')
		self.get_item_input_box().send_keys(Keys.ENTER)
		self.wait_for_row_in_list_table('1:Banter too thick')
		self.get_item_input_box().send_keys('Banter too thick')
		self.get_item_input_box().send_keys(Keys.ENTER)

		self.wait_for(lambda: self.assertTrue(
			self.get_error_element().is_displayed()
		))

		# She start enter new value to the input sield
		self.get_item_input_box().send_keys('a')

		# She is happy, because error message hide
		self.wait_for(lambda: self.assertFalse(
			self.get_error_element().is_displayed()
		))


